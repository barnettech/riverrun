README FILE

Bagel River Raft Adventures
---------------------------

* Open the first scene called introScene, there are 3 scenes in total.  Run the first scene in Unity.

* Hit the space bar to continue, notice on this screen I'm programmatically pulling in the latest news from my website www.barnettech.com via RSS.  Also please notice the highscore is featured on this screen, once there is a high score to post.

* Once the game starts after hitting the space bar you'll be brought into scene RollABall (originally the main character was just a ball).  The game is kid friendly, I have a 7 and a 10 year old ;) so your raft is a bagel, and it shoots cream cheese balls!  You will be collecting 10 coins or shooting at least 10 bagels.  Beware there are birds trying to eat your bagel, and rogue bagels need cream cheese balls, you can shoot the cream cheese balls at the bagels to shmear 10 bagels with cream cheese.  Collect the 10 coins or shmear 10 bagels, and you've defeated the level.  Best time on Level 1 is recorded and shows up recorded and saved to keep track of best score. You can move with arrow keys, or AWSD.  You can shoot using the mouse button, and you can control where you shoot by pointing the mouse (uses Raycasting).  

* Once you enter level 2, "GaiaScene", your bagel raft will have landed on an exotic island, with crazy birds, who will attack you, we don't really know why, but I did say they were crazy birds, reason enough.  Avoid the birds and collect 10 more coins to defeat this level.  Use the AWSD keys to move around.  No shooting on this level, if you see a bird just run!

I hope the game is fun, and satisfies all requirements.  There's an educational piece half implemented where I will pull in questios from an RSS feed, and it can be used by teachers to gamify learning assignments (K-12).  I didn't remove this code, but will implement the feature after the class, for a potential client at a school that I already have.  Students instead of dying will answer a question, if successful, they'll be able to continue playing, otherwise the crazed birds, or rogue bagels will unfortunately have their way.
