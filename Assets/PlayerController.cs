﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.Internal;

public class PlayerController : MonoBehaviour {
    public GameObject[] prefabs;
    public float speed;
    public Vector3 jump;
    public Text countText;
    public Text winText;
    public Text bagelsShmeared;
    public Text timerLabel;
    public Text highscore;
    private float time;
    private float startTime;
    public GameObject bulletPrefab;
    public Transform bulletSpawn;
    
    private bool restart = false;

    private Rigidbody rb;
    private int count;
    public static int numWithShmear;
    private GameObject playerPosition;
    private bool finishedthelevel = false;

    private Ray mouseRay;
    private RaycastHit hit;
    
    void Start ()
    {
        // PlayerPrefs.DeleteAll();
        playerPosition = GameObject.Find("Player");
        rb = GetComponent<Rigidbody>();
        jump = new Vector3(0.0f, 2.0f, 0.0f);
        count = 0;
        numWithShmear = 0;
        SetCountText ();
        winText.text = "";
        startTime = Time.time;
        highscore.text = "Highscore: " + PlayerPrefs.GetFloat ("HighScore", 0).ToString();
    }
    
    void Update() {
      bagelsShmeared.text = "Bagels Shmeared: " + numWithShmear.ToString ();
      if ((Input.GetKeyDown (KeyCode.Return)) || Input.GetMouseButtonDown(0)) {
          mouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);
          Debug.Log(mouseRay);
          Debug.DrawRay (mouseRay.origin, mouseRay.direction * 50, Color.red);
          
          Debug.Log("direction is " + mouseRay.direction);
        Fire(mouseRay.direction);
          
      }
      if (playerPosition.transform.position.y < 5 && playerPosition.transform.position.y < 1 && Input.GetButtonDown("Jump") && finishedthelevel == false) {
         print("space key was pressed");
         rb.AddForce(jump * 10.0f, ForceMode.Impulse);
      }
      if (Input.GetKey ("up") ||    Input.GetKey(KeyCode.W)) {
         rb.AddForce(Vector3.forward * 50);
      } 
      if (Input.GetKey ("right") || Input.GetKey(KeyCode.D)) {
         rb.AddForce(Vector2.right * 50);
      } 
      if (Input.GetKey ("left") || Input.GetKey(KeyCode.A)) {
         rb.AddForce(Vector2.left * 50);
      } 
      float t = Time.time - startTime;
      string minutes = ((int)t / 60).ToString ();
      string seconds = (t % 60).ToString ("f2");
      timerLabel.text = minutes + ":" + seconds;
 
      if (restart)
        {
            if (Input.GetKeyDown (KeyCode.R))
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().name );
            }
       }
       if (Input.GetButtonDown("Jump") && finishedthelevel) {
              Debug.Log("the jump key was pressed");
              winText.text = "You win! Please wait loading level 2....";
              SceneManager.LoadScene("GaiaScene");
       }
    }

    void FixedUpdate ()
    {
        float moveHorizontal = Input.GetAxis ("Horizontal");
        float moveVertical = Input.GetAxis ("Vertical");

        //Vector3 movement = new Vector3 (moveHorizontal, 5.0f, 0.0f);

        //rb.AddForce (movement * speed);
    }

    void OnTriggerEnter(Collider other) 
    {
        if (other.gameObject.CompareTag ("Pick Up"))
        {
            other.gameObject.SetActive (false);
            count = count + 1;
            SetCountText ();
            GetComponents<AudioSource>()[0].Play();
        }
        if (other.gameObject.CompareTag ("bird") && finishedthelevel == false) {
          SetLoseText2();
        }
        
        if (other.gameObject.CompareTag ("bagel") && finishedthelevel == false) {
          SetLoseText();
        }
        
    }

    void SetCountText ()
    {
        countText.text = "Count: " + count.ToString ();
        if (count >= 10 || numWithShmear >= 10)
        {
            winText.text = "You Win! Press the jump key to move on to Level 2";
            finishedthelevel = true;
            float tt = Time.time - startTime;
            if(!PlayerPrefs.HasKey("HighScore")) {
                 PlayerPrefs.SetFloat ("HighScore", tt);
                 PlayerPrefs.Save();
                 highscore.text = "Highscore: " + tt;
              }
            else if (tt < PlayerPrefs.GetFloat ("HighScore", float.MaxValue)) {
                PlayerPrefs.SetFloat ("HighScore", tt);
                highscore.text = "New Highscore is: " + tt;
                PlayerPrefs.Save();
            } 
        }
    }
    
    void SetLoseText ()
    {
        
        winText.text = "You Lose!  You seem to have collided head on with a bagel, strange as that sounds. Press R to restart.";
        restart = true;
        
    }
    
    void SetLoseText2 ()
    {
        
        winText.text = "You Lose, the birds got to your bagel, ate some, and it's no longer usable! Press R to restart.";
        restart = true;
        
    }
    
    void Fire(Vector3 mouseraydirection)
{
    // Create the Bullet from the Bullet Prefab
    var bullet = (GameObject)Instantiate (
        bulletPrefab,
        bulletSpawn.position,
        Quaternion.identity);
        
    bullet.transform.rotation = Quaternion.LookRotation(mouseraydirection);

   var rb = bullet.GetComponent<Rigidbody>();

    rb.AddForce(mouseraydirection * 1000);

    // Destroy the bullet after 2 seconds
    Destroy(bullet, 2.0f);
}
    
}
