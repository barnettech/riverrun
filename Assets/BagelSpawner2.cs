﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BagelSpawner2 : MonoBehaviour {

	public GameObject[] bagelPrefabs;
    private GameObject playerPosition;
    Vector3 lastPosition;
	// Use this for initialization
	void Start () {
      playerPosition = GameObject.Find("Player");
      lastPosition = playerPosition.transform.position;
      for (int i = 0; i < 800; i++) {
        Instantiate(bagelPrefabs[Random.Range(0, bagelPrefabs.Length)], new Vector3(Random.Range(1024, -1024), 60.89472f, Random.Range(1024, -1024)), Quaternion.identity);
      }
	}
	
	// Update is called once per frame
	void Update () {
      GameObject[] argo = GameObject.FindGameObjectsWithTag("bagel");
      foreach (GameObject go in argo) {
        //GameObject playerCurrent = GameObject.FindGameObjectWithTag ("Player");
        float dist = Vector3.Distance(go.transform.position, playerPosition.transform.position);
        //Debug.Log("Distance to other: " + dist);
        if(dist < 20) {
          GameObject playerCurrent = GameObject.FindGameObjectWithTag ("Player");
          Vector3 newPos = new Vector3(playerCurrent.transform.position.x, playerCurrent.transform.position.y, playerCurrent.transform.position.z - 1); 
          go.transform.position = Vector3.Lerp(go.transform.position, newPos, Random.Range(0.45f, 1) * Time.deltaTime);
        }
      }
		
	}
}
