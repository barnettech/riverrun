﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gaiaCoinSpawner : MonoBehaviour {
    public GameObject[] prefabs;
	// Use this for initialization
	void Start () {
        for (int i = 0; i < 100; i++) {
		  Instantiate(prefabs[Random.Range(0, prefabs.Length)], new Vector3(Random.Range(1024, -1024), 60.89472f, Random.Range(1024, -1024)), Quaternion.identity);
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
