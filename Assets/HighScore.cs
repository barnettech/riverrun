﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighScore : MonoBehaviour {
    public Text highscore;
	// Use this for initialization
	void Start () {
		highscore.text = "Level 1 Highscore: " + PlayerPrefs.GetFloat ("HighScore", 0).ToString() + " seconds";
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
