﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BagelSpawner : MonoBehaviour {
    public GameObject[] bagelPrefabs;
	public float spawnDistance;
    private GameObject playerLastPosition;
    private GameObject playerPosition;
    private GameObject groundPosition;
    private GameObject activePrefab;
    float distanceTravelled = 0;
    Vector3 lastPosition;
    int tileCount = 0;
    public List<GameObject> pickuplist;
    
	// Use this for initialization
	void Start () {

		// infinite coin spawning function, asynchronous
		//StartCoroutine(SpawnGround());
        playerPosition = GameObject.Find("Player");
        groundPosition = GameObject.Find("Ground1");
        lastPosition = playerPosition.transform.position;
	}

	// Update is called once per frame
	void Update () {
      distanceTravelled += Vector3.Distance(playerPosition.transform.position, lastPosition);
      lastPosition = playerPosition.transform.position;
      // Debug.Log("tileCount is " + tileCount + " and distanceTravelled is " + distanceTravelled);
      if(distanceTravelled > 10 * tileCount ) {
          tileCount++;
          for (int i = 0; i < 4; i++) {
            GameObject activePrefab = Instantiate(bagelPrefabs[Random.Range(0, bagelPrefabs.Length)], new Vector3(groundPosition.transform.position.x + (Random.Range(-4, 4)), groundPosition.transform.position.y - (45 * tileCount), groundPosition.transform.position.z + Random.Range(40,64) * tileCount), Quaternion.identity) as GameObject;
            activePrefab.transform.rotation = groundPosition.transform.rotation;
            pickuplist.Add(activePrefab);
          }
          GameObject gameObjectToRemove1 = pickuplist[1];
          if(pickuplist.Count > 20 && gameObjectToRemove1.transform.position.z < playerPosition.transform.position.z) {
            GameObject gameObjectToRemove = pickuplist[0];
            pickuplist.Remove(gameObjectToRemove);
            Destroy(gameObjectToRemove);
          }
      }
      GameObject[] argo = GameObject.FindGameObjectsWithTag("bagel");
      foreach (GameObject go in argo) {
        float dist = Vector3.Distance(go.transform.position, playerPosition.transform.position);
        //Debug.Log("Distance to other: " + dist);
        if(dist < 100) {
          if(go.transform.position.y < groundPosition.transform.position.y + 5) {
            go.transform.transform.Translate(Vector3.up * Random.Range(0.45f, 2) * Time.deltaTime);
          }
          GameObject playerCurrent = GameObject.FindGameObjectWithTag ("Player");
          Vector3 newPos = new Vector3(playerCurrent.transform.position.x, playerCurrent.transform.position.y, playerCurrent.transform.position.z - 10); 
          go.transform.position = Vector3.Lerp(go.transform.position, newPos, Random.Range(0.45f, 1) * Time.deltaTime);
        }
      }
        
    }
    
}