﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GaiaPlayerController : MonoBehaviour {
    private int count;
    public Text Score;
    public Text OpeningText;
	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        if(Input.GetButtonDown("Jump")) {
            OpeningText.text = "";
            GetComponents<AudioSource>()[1].Play();
        }
		
	}
    
    void OnTriggerEnter(Collider other) 
    {
        GetComponents<AudioSource>()[0].Play();
        if (other.gameObject.CompareTag ("Pick Up"))
        {
            other.gameObject.SetActive (false);
            count = count + 1;
            GetComponents<AudioSource>()[1].Play();
            SetCountText ();
        }
        
        if (other.gameObject.CompareTag ("bird"))
        {
            SetLoseText ();
        }
    }

    void SetCountText ()
    {
        Score.text = "Score: " + count.ToString ();
        if (count >= 10)
        {
            Score.text = "You Win!";
        }
    }
    void SetLoseText ()
    {
        Score.text = "You Lose!";
    }
}
